#! /usr/bin/env python3
import datetime
import json
import time
import urllib.request
from urllib.error import URLError, HTTPError
import sys
import utils
import dashboards
from prometheus_client import Counter, Gauge, start_http_server
from dotenv import load_dotenv
import os
import logging
load_dotenv()


logging.basicConfig(filename="/tmp/aggregator_log.txt",level=logging.INFO, format='%(asctime)s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %H:%M:%S')

load_dotenv()


"""
Configuration for grafana annotations api, we need the dashboardUID, panelId, grafana_url (which is the url ov the vm
plus the port ) and the token that is generated for the specific dasboard

"""
# Hlt_accept_fraction = hlt_rate_Hz / tcds_section_rates_trg_rate_total
# But needs protection when tcds_section_rates_trg_rate_total = 0
# Add a curve to the to the “production dashboard” panel: History (LS based)
# og scale in % on the left (same as dead_time)

url = os.getenv("VM_URL")
grafana_url = os.getenv("GRAFANA_URL")
token = os.getenv("TOKEN")
dashboardUID = os.getenv("PRODUCTION_DASHUID")
tcds_dashboardUID = os.getenv("TCDS_DASHUID")
summary_dashboardUID = os.getenv("SUMMARY_DASHUID")


# aggregator produces a snapshot every 2-3
poll_time_seconds = 2
# daqview defaults to waiting 2 and skipping 3 seconds to maximize likelihood of getting a new snapshot
# accumulate the trg_total for all lumi-sections in a run.: ls getes
# reset to 1 at start of new run
tcds_section_counts_trg_total_accumulated = 0
last_tcds_section_counts_ls = -1
last_tcds_section_rates_ls = -1
last_tcds_section_deadtimes_ls = -1
last_run_number = -1

gauge_system_time = Gauge(
    "gauge_system_time", "The time of system when lv1 is entered")
gauge_snapshot_timestamp = Gauge(
    "gauge_snapshot_timestamp", "The timestamp from the snapshot")
gauge_not_new_snapsot = Gauge(
    "not_new_snapshot", "Tis Value is set when no new snaposhot")

def work_snapshot(snapshot, first_run):

    

    global last_tcds_section_counts_ls
    global last_tcds_section_rates_ls
    global last_tcds_section_deadtimes_ls
    global tcds_section_deadtimes_total
    global last_run_number
    global tcds_section_counts_trg_total_accumulated
    global hlt_accept_fraction
    global feds_dict_sec
    global feds_dict_back_pre_sec
    global feds_dict_bifi_back_pre_sec
    global feds_acc_max_delta_values
    # on linear scale
    gauge_not_new_snapsot.set(0)
    dashboards.gauge_last_update_timestamp.set(snapshot["lastUpdate"])
    run_number = snapshot["runNumber"]
    # checking if we have a new run number in order to send the annotations
    if run_number != last_run_number:
        
        last_run_number = run_number
        dashboards.gauge_run_number.set(run_number)
      
        first_run = True
        
        feds_dict_sec = utils.fetch_ref_ttcp(snapshot, "empty_dictionary")
        feds_dict_back_pre_sec = utils.fetch_ref_ttcp(snapshot,"empty_dictionary")
        feds_dict_bifi_back_pre_sec = utils.fetch_ref_ttcp(snapshot,"empty_dictionary")
        # print("Changed first run to true")
        
        utils.make_post_request(utils.create_encoded_body("POST", dashboardUID, 2, "new_run","New run number: " + str(run_number), int(time.time()*1000)), grafana_url, token)
        utils.make_post_request(utils.create_encoded_body("POST", dashboardUID, 3, "new_run","New run number: " + str(run_number), int(time.time()*1000)), grafana_url, token)
        utils.make_post_request(utils.create_encoded_body("POST", tcds_dashboardUID, 2, "new_run","New run number: " + str(run_number), int(time.time()*1000)), grafana_url, token)
        utils.make_post_request(utils.create_encoded_body("POST", tcds_dashboardUID, 4, "new_run","New run number: " + str(run_number), int(time.time()*1000)), grafana_url, token)
        utils.make_post_request(utils.create_encoded_body("POST", summary_dashboardUID, 2, "new_run","New run number: " + str(run_number), int(time.time()*1000)), grafana_url, token)
    

    # initialize the dictionary with the seconds with -1
    feds_dict = utils.fetch_srcid_for_each_ttcp(
        snapshot, utils.fetch_ref_ttcp(snapshot, "empty_list"))

    # creating the dictionary that I will save the accMax value per partion for the frlAccSlink, back pressure and bifi

    
    if first_run == True:
        feds_dict_sec.update(utils.initialize_feds_rates_dict(feds_dict_sec,snapshot))
        feds_dict_back_pre_sec.update(utils.initialize_feds_rates_dict(feds_dict_back_pre_sec,snapshot))
        feds_dict_bifi_back_pre_sec.update(utils.initialize_feds_rates_dict(feds_dict_bifi_back_pre_sec,snapshot))
        feds_acc_max_delta_values = utils.fetch_ref_ttcp(snapshot,"empty_dictionary")

        for fed in feds_acc_max_delta_values:
            feds_acc_max_delta_values.update({fed:{"acc_max_delta_frlAccSlink": 0, "acc_max_delta_frlAccBack":0, "acc_max_delta_frlAccBIFI": 0}})   
    
    # print(feds_dict_sec)
    # looping through the feds_dict items, getting name and srcIdexpected for labels in the metrics
   
    for key, value in feds_dict.items():
      
        if len(value) > 0:  
            sum_of_all_src_ids_accFullSec = 0
            sum_of_all_src_ids_accBackpressure = 0
            sum_of_all_src_ids_accBIFIBackpressure = 0
            current_delta_slink_full_max = 0
            current_delta_acc_back_pressure_max = 0
            current_delta_acc_bifi_back_pressure_max = 0
            max_fed_accSlink = 0
            max_fed_accBack = 0
            max_fed_accBifi = 0
            new_values_from_fed  = dict()   
            for i in range(0, len(value)):
                new_values_from_fed = utils.fetch_fed_values(snapshot,value[i])


                dashboards.gauge_fed_frl_tcp_stream_timestamp.labels(key,value[i]).set(new_values_from_fed["frl_tcp_stream_timestamp"])
                dashboards.gauge_fed_frl_input_stream_timestamp.labels(key,value[i]).set(new_values_from_fed["frl_input_stream_timestamp"])

                dashboards.gauge_fed_eventcounter.labels(key,value[i]).set(new_values_from_fed["eventCounter"])
                dashboards.gauge_fed_frl_bifi_used.labels(key,value[i]).set(new_values_from_fed["frl_bifi_used"])
                dashboards.gauge_fed_frl_tcp_stat_persist_closedwnd.labels(key,value[i]).set(new_values_from_fed["frl_tcp_stat_persist_closedwnd"])
                dashboards.gauge_fed_frl_tcp_stat_current_wnd.labels(key,value[i]).set(new_values_from_fed["frl_tcp_stat_current_wnd"])

                dashboards.gauge_ttcp_frl_acc_back_pres_sec.labels(key,value[i]).set(new_values_from_fed["frl_AccBackpressureSeconds"])
                dashboards.gauge_ttcp_frl_acc_sec.labels(key, value[i]).set(new_values_from_fed["frl_AccSlinkFullSec"])
                dashboards.gauge_ttcp_frl_acc_bifi_pre_sec.labels(key, value[i]).set(new_values_from_fed["frl_AccBIFIBackpressureSeconds"])
                dashboards.gauge_ttcp_frl_slink_rcv_fifo_cnt_transitions.labels(key,value[i]).set(new_values_from_fed["frl_slink_rcv_fifo_cnt_transitions"])
                dashboards.gauge_ttcp_frl_tcpstream_bifi_cnt_transitions.labels(key,value[i]).set(new_values_from_fed["frl_tcpstream_bifi_cnt_transitions"])
                
                #  fmm counters
                fmm_counters = utils.fetch_fmm_counters(snapshot,value[i])
               
                if fmm_counters != -1:
                    dashboards.gauge_ttcp_fmm_integral_time_ready_bx.labels(key,value[i]).set(fmm_counters[0])
                    dashboards.gauge_ttcp_fmm_integral_time_busy_bx.labels(key,value[i]).set(fmm_counters[1])
                    dashboards.gauge_ttcp_fmm_integral_time_warning_bx.labels(key,value[i]).set(fmm_counters[2])
                    dashboards.gauge_ttcp_fmm_integral_time_error_bx.labels(key,value[i]).set(fmm_counters[3])
                    dashboards.gauge_ttcp_fmm_integral_time_oss_bx.labels(key,value[i]).set(fmm_counters[4])
                    dashboards.gauge_ttcp_fmm_time_tag_bx.labels(key,value[i]).set(fmm_counters[5])
                    

              
                warning = utils.fetch_from_feds_percents(snapshot,value[i])[0]
                busy = utils.fetch_from_feds_percents(snapshot,value[i])[1]
                
                dashboards.gauge_ttcp_percent_warning.labels(key,value[i]).set(warning)
                dashboards.gauge_ttcp_percent_busy.labels(key,value[i]).set(busy)
                
                frl_AccSlinkFullSec = new_values_from_fed["frl_AccSlinkFullSec"]
                frl_AccBackpressureSeconds = new_values_from_fed["frl_AccBackpressureSeconds"]
                frl_AccBIFIBackpressureSeconds = new_values_from_fed["frl_AccBIFIBackpressureSeconds"]
              

                # this means its the first time so all of them have -1
                if feds_dict_sec[key][value[i]] == -1:

                    feds_dict_sec[key][value[i]] = frl_AccSlinkFullSec
                    feds_dict_back_pre_sec[key][value[i]] = frl_AccBackpressureSeconds
                    feds_dict_bifi_back_pre_sec[key][value[i]] = frl_AccBIFIBackpressureSeconds
                    
                    rate_slink_full = 0
                    rate_back_pre_sec = 0
                    rate_bifi_back_pre_sec =0
                    # print(key,value[i],feds_dict_sec[key][value[i]],feds_dict_back_pre_sec[key][value[i]],feds_dict_bifi_back_pre_sec[key][value[i]])
         

                else:
                    rate_slink_full = utils.calculate_rate(frl_AccSlinkFullSec,feds_dict_sec[key][value[i]],delta_time)
                    rate_back_pre_sec = utils.calculate_rate(frl_AccBackpressureSeconds,feds_dict_back_pre_sec[key][value[i]],delta_time)
                    rate_bifi_back_pre_sec = utils.calculate_rate(frl_AccBIFIBackpressureSeconds,feds_dict_bifi_back_pre_sec[key][value[i]],delta_time)

                    feds_dict_sec[key][value[i]] = frl_AccSlinkFullSec
                    feds_dict_back_pre_sec[key][value[i]] = frl_AccBackpressureSeconds
                    feds_dict_bifi_back_pre_sec[key][value[i]] = frl_AccBIFIBackpressureSeconds
                    
                sum_of_all_src_ids_accFullSec += utils.add_to_the_sum(frl_AccSlinkFullSec)
                sum_of_all_src_ids_accBackpressure += utils.add_to_the_sum(frl_AccBackpressureSeconds)
                sum_of_all_src_ids_accBIFIBackpressure += utils.add_to_the_sum(frl_AccBIFIBackpressureSeconds)
               
                current_delta_slink_full_max, max_fed_accSlink = utils.update_current_max(rate_slink_full * delta_time,current_delta_slink_full_max,value[i],max_fed_accSlink) 
                current_delta_acc_back_pressure_max, max_fed_accBack = utils.update_current_max(rate_back_pre_sec *delta_time,current_delta_acc_back_pressure_max,value[i],max_fed_accBack)
                current_delta_acc_bifi_back_pressure_max,max_fed_accBifi = utils.update_current_max(rate_bifi_back_pre_sec *delta_time,current_delta_acc_bifi_back_pressure_max,value[i],max_fed_accBifi)
                
                        
                dashboards.gauge_ttcp_frl_rate_slink_full.labels(key, value[i]).set(rate_slink_full)
                dashboards.gauge_ttcp_frl_rate_bifi_pre_sec.labels(key,value[i]).set(rate_bifi_back_pre_sec)
                dashboards.gauge_ttcp_frl_rate_back_pre_sec.labels(key,value[i]).set(rate_back_pre_sec)
        
        # print(max_fed_accBifi)
        dashboards.gauge_ttcp_max_incremented_delta_value_frl_acc_sec.labels(key).set(max_fed_accSlink)
        dashboards.gauge_ttcp_max_incremented_delta_value_back_pre_sec.labels(key).set(max_fed_accBack)
        dashboards.gauge_ttcp_max_incremented_delta_value_bifi_back_pre_sec.labels(key).set(max_fed_accBifi)  

        # 
        feds_acc_max_delta_values[key]["acc_max_delta_frlAccSlink"] +=  current_delta_slink_full_max
        feds_acc_max_delta_values[key]["acc_max_delta_frlAccBack"] +=  current_delta_acc_back_pressure_max
        feds_acc_max_delta_values[key]["acc_max_delta_frlAccBIFI"] += current_delta_acc_bifi_back_pressure_max

        dashboards.gauge_ttcp_acc_max_incremented_delta_value_frl_acc_sec.labels(key).set(feds_acc_max_delta_values[key]["acc_max_delta_frlAccSlink"])
        dashboards.gauge_ttcp_acc_max_incremented_delta_value_back_pre_sec.labels(key).set(feds_acc_max_delta_values[key]["acc_max_delta_frlAccBack"])
        dashboards.gauge_ttcp_acc_max_incremented_delta_value_bifi_back_pre_sec.labels(key).set(feds_acc_max_delta_values[key]["acc_max_delta_frlAccBIFI"])
        
        
        dashboards.gauge_ttcp_frl_acc_sec_sum_per_partition.labels(key).set(sum_of_all_src_ids_accFullSec)
        dashboards.gauge_ttcp_frl_acc_bifi_pre_sum_per_partition.labels(key).set(sum_of_all_src_ids_accBIFIBackpressure)
        dashboards.gauge_ttcp_frl_acc_back_pre_sum_per_partition.labels(key).set(sum_of_all_src_ids_accBackpressure)
        
    current_lv1_rate_khz = snapshot['fedBuilderSummary']['rate'] / 1000
    dashboards.gauge_lv1_rate_khz.set(current_lv1_rate_khz)
    # on linear scale
    accumulated_lv1_event_count = snapshot['buSummary']['numEvents']
    dashboards.gauge_accumulated_lv1_events.set(accumulated_lv1_event_count)
    gauge_system_time.set(datetime.datetime.now().timestamp())
    # on log scale
    # deadtime_instant_percent = snapshot['tcdsGlobalInfo']['deadTimesInstant']['total']
    # to add to the panels >=0
    deadtime_instant_percent = utils.fetch_tcds_global_info_deadtimes_instant(snapshot['tcdsGlobalInfo']['deadTimesInstant'],"total")
    dashboards.gauge_deadtime_instant_percent.set(deadtime_instant_percent)

    # tcds_section_counts_ls = snapshot['tcdsGlobalInfo']['sectionNumber_counts']
    tcds_section_counts_ls = utils.fetch_tcds_global_info(snapshot['tcdsGlobalInfo'],'sectionNumber_counts')

    # ----------------------------------------------------------------------------------------------------
    # info for the new dasboard panel Rates

    dashboards.gauge_tcds_instant_trg_rate_physics_khz.set(
        snapshot['tcdsGlobalInfo']['triggerRatesInstant']['trg_rate_tt_values'][1] / 1000)
    dashboards.gauge_tcds_instant_trg_rate_total_khz.set(
        snapshot['tcdsGlobalInfo']['triggerRatesInstant']['trg_rate_total'] / 1000)
    dashboards.gauge_tcds_instant_trg_rate_seq_calib_khz.set(
        snapshot['tcdsGlobalInfo']['triggerRatesInstant']['trg_rate_tt_values'][2] / 1000)
    dashboards.gauge_tcds_instant_trg_rate_random_khz.set(
        snapshot['tcdsGlobalInfo']['triggerRatesInstant']['trg_rate_tt_values'][3] / 1000)

    # -------------------------------------------------------------------------------------------------------
    # second panel dead times
    dashboards.gauge_tcds_instant_deadtimes_total.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['total'])
    dashboards.gauge_tcds_instant_deadtimes_bx_mask.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['bx_mask'])
    dashboards.gauge_tcds_instant_deadtimes_trg_rules.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['trg_rules'])
    dashboards.gauge_tcds_instant_deadtimes_calib.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['calib'])
    dashboards.gauge_tcds_instant_deadtimes_daq_bp.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['daq_bp'])
    dashboards.gauge_tcds_instant_deadtimes_retri.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['retri'])
    dashboards.gauge_tcds_instant_deadtimes_tts.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['tts'])
    dashboards.gauge_tcds_instant_deadtimes_apve.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['apve'])
    dashboards.gauge_tcds_instant_deadtimes_fw_pause.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['fw_pause'])
    dashboards.gauge_tcds_instant_deadtimes_sw_pause.set(
        snapshot['tcdsGlobalInfo']['deadTimesInstant']['sw_pause'])

    # ----------------------------------------------------------------------------------------------------------
    tcds_section_rates_trg_rate_total = snapshot['tcdsGlobalInfo']['trg_rate_total']

    if snapshot['hltRate'] == None:
        dashboards.gauge_hlt_rate_Hz.set(-1)
        dashboards.gauge_hlt_accept_fraction.set(0)

    else:
        hlt_rate_Hz = snapshot['hltRate']
        dashboards.gauge_hlt_rate_Hz.set(hlt_rate_Hz)

        if utils.check_zero_value(tcds_section_rates_trg_rate_total) == True:
            dashboards.gauge_hlt_accept_fraction.set(-1)
        else:
            dashboards.gauge_hlt_accept_fraction.set(
                (hlt_rate_Hz / tcds_section_rates_trg_rate_total) * 100)
    # GBps
    if snapshot['hltBandwidth'] == None:
        dashboards.gauge_hlt_out_thru_GBps.set(-1)
    else:
        dashboards.gauge_hlt_out_thru_GBps.set(
            snapshot['hltBandwidth'] / 1000000000)

    if tcds_section_counts_ls == 1:
        tcds_section_counts_trg_total_accumulated = 0

    tcds_section_counts_trg_cnt_total = snapshot['tcdsGlobalInfo']['trg_cnt_total']
    tcds_section_rates_ls = snapshot['tcdsGlobalInfo']['sectionNumber_rates'] / 1000
    # store if chnaged
    tcds_section_deadtimes_ls = snapshot['tcdsGlobalInfo']['sectionNumber_deadtimes']

    if tcds_section_counts_ls != last_tcds_section_counts_ls:

        tcds_section_deadtimes_total = snapshot['tcdsGlobalInfo']['deadTimes']['total']

        last_tcds_section_counts_ls = tcds_section_counts_ls
        dashboards.gauge_tcds_section_counts_ls.set(tcds_section_counts_ls)

        tcds_section_counts_trg_total_accumulated += tcds_section_counts_trg_cnt_total

        dashboards.gauge_tcds_section_counts_trg_total_accumulated.set(
            tcds_section_counts_trg_total_accumulated)
        dashboards.gauge_tcds_section_counts_trg_cnt_total.set(
            tcds_section_counts_trg_cnt_total)
        #  2 sets for prometheus
        # store in prometheus tcds

    if tcds_section_rates_ls != last_tcds_section_rates_ls:

        last_tcds_section_rates_ls = tcds_section_rates_ls
        dashboards.gauge_tcds_section_rates_ls.set(tcds_section_rates_ls)

        tcds_section_rates_trg_rate_total = snapshot['tcdsGlobalInfo']['trg_rate_total'] / 1000
        dashboards.gauge_tcds_section_rates_trg_rate_total.set(
            tcds_section_rates_trg_rate_total)

    if tcds_section_deadtimes_ls != last_tcds_section_deadtimes_ls:

        last_tcds_section_deadtimes_ls = tcds_section_deadtimes_ls
        dashboards.gauge_tcds_section_deadtimes_total.set(
            tcds_section_deadtimes_total)
        dashboards.gauge_tcds_section_deadtime_ls.set(
            tcds_section_deadtimes_ls)

    for ttc_partition in snapshot['ttcPartitions']:
        label = ttc_partition["name"]

        if ttc_partition["masked"] == False:

            if ttc_partition["tcds_pm_ttsState"][-1].isnumeric() == False:
                tcds_pm_ttsState = -1
            else:
                # maybe we have to check if is more than one digit
                tcds_pm_ttsState = int(ttc_partition["tcds_pm_ttsState"][-1])

            ttc_partition_warning_and_busy = int(
                ttc_partition["percentWarning"]) + int(ttc_partition["percentBusy"])
            dashboards.gauge_tcds_pm_ttsState.labels(
                label).set(tcds_pm_ttsState)
            dashboards.gauge_ttc_partition_warning_and_busy.labels(
                label).set(ttc_partition_warning_and_busy)

        else:
            dashboards.gauge_tcds_pm_ttsState.labels(label).set(-1)
            dashboards.gauge_ttc_partition_warning_and_busy.labels(
                label).set(0)

    # those 2 loops get the fed builder name which is only to the rus and depending on the hostname
    # they map it with the bus, so in the metrics also the bus can have the fed builder
    bus_rus_map = {}
    bu_index = 0
    for bu in snapshot["bus"]:
        name = ((bu["hostname"].split("-")[1]+"-" +
                bu["hostname"].split("-")[2]).split(".")[0])[2:]
        bus_rus_map.update({name: [bu_index, 0, ""]})
        bu_index += 1

    ru_index = 0
    for ru in snapshot["rus"]:
        fed_builder = ru["ref_fedBuilder"].split('_')[1]
        hostname = ((ru["hostname"].split("-")[1]+"-" +
                    ru["hostname"].split("-")[2]).split(".")[0])[2:]
        bus_rus_map[hostname] = [
            bus_rus_map[hostname][0], ru_index, fed_builder]
        ru_index += 1

    for bu in snapshot["bus"]:
        name = ((bu["hostname"].split("-")[1]+"-" +
                bu["hostname"].split("-")[2]).split(".")[0])[2:]

        if bu["stateName"] != "Enabled":
            dashboards.gauge_bu_rate.labels(name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_throughput.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_event_size_mean.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_event_size_stddev.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_event_num_events.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_event_num_events_in_bu.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_priority.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_requests_sent.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_requests_used.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_requests_blocked.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_fus_hlt.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_fus_crashed.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_fus_stale.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_fus_cloud.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_ram_disk_usage.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_ram_disk_total.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_files.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_lumisections_with_files.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_current_lumisection.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_lumisections_for_hlt.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_num_lumisections_out_hlt.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_fu_output_bandwidth_in_mb.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_fragment_count.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_nb_corrupted_events.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_nb_events_missing_data.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_nb_events_with_crc_errors.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_nb_total_resources.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_request_rate.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_request_retry_rate.labels(
                name, bus_rus_map[name][2]).set(-1)
            dashboards.gauge_bu_slowest_ru_tid.labels(
                name, bus_rus_map[name][2]).set(-1)
        else:
            dashboards.gauge_bu_rate.labels(
                name, bus_rus_map[name][2]).set(bu["rate"] / 1000)
            dashboards.gauge_bu_throughput.labels(
                name, bus_rus_map[name][2]).set(bu["throughput"] / 1000000)
            dashboards.gauge_bu_event_size_mean.labels(
                name, bus_rus_map[name][2]).set(bu["eventSizeMean"] / 1000)
            dashboards.gauge_bu_event_size_stddev.labels(
                name, bus_rus_map[name][2]).set(bu["eventSizeStddev"] / 1000)
            dashboards.gauge_bu_event_num_events.labels(
                name, bus_rus_map[name][2]).set(bu["numEvents"])
            dashboards.gauge_bu_event_num_events_in_bu.labels(
                name, bus_rus_map[name][2]).set(bu["numEventsInBU"])
            dashboards.gauge_bu_priority.labels(
                name, bus_rus_map[name][2]).set(bu["priority"])
            dashboards.gauge_bu_num_requests_sent.labels(
                name, bus_rus_map[name][2]).set(bu["numRequestsSent"])
            dashboards.gauge_bu_num_requests_used.labels(
                name, bus_rus_map[name][2]).set(bu["numRequestsUsed"])
            dashboards.gauge_bu_num_requests_blocked.labels(
                name, bus_rus_map[name][2]).set(bu["numRequestsBlocked"])
            dashboards.gauge_bu_num_fus_hlt.labels(
                name, bus_rus_map[name][2]).set(bu["numFUsHLT"])
            dashboards.gauge_bu_num_fus_crashed.labels(
                name, bus_rus_map[name][2]).set(bu["numFUsCrashed"])
            dashboards.gauge_bu_num_fus_stale.labels(
                name, bus_rus_map[name][2]).set(bu["numFUsStale"])
            dashboards.gauge_bu_num_fus_cloud.labels(
                name, bus_rus_map[name][2]).set(bu["numFUsCloud"])
            dashboards.gauge_bu_ram_disk_usage.labels(
                name, bus_rus_map[name][2]).set(bu["ramDiskUsage"])
            dashboards.gauge_bu_ram_disk_total.labels(
                name, bus_rus_map[name][2]).set(bu["ramDiskTotal"])
            dashboards.gauge_bu_num_files.labels(
                name, bus_rus_map[name][2]).set(bu["numFiles"])
            dashboards.gauge_bu_num_lumisections_with_files.labels(
                name, bus_rus_map[name][2]).set(bu["numLumisectionsWithFiles"])
            dashboards.gauge_bu_current_lumisection.labels(
                name, bus_rus_map[name][2]).set(bu["currentLumisection"])
            dashboards.gauge_bu_num_lumisections_for_hlt.labels(
                name, bus_rus_map[name][2]).set(bu["numLumisectionsForHLT"])
            dashboards.gauge_bu_num_lumisections_out_hlt.labels(
                name, bus_rus_map[name][2]).set(bu["numLumisectionsOutHLT"])
            dashboards.gauge_bu_fu_output_bandwidth_in_mb.labels(
                name, bus_rus_map[name][2]).set(bu["fuOutputBandwidthInMB"])
            dashboards.gauge_bu_fragment_count.labels(
                name, bus_rus_map[name][2]).set(bu["fragmentCount"])
            dashboards.gauge_bu_nb_corrupted_events.labels(
                name, bus_rus_map[name][2]).set(bu["nbCorruptedEvents"])
            dashboards.gauge_bu_nb_events_missing_data.labels(
                name, bus_rus_map[name][2]).set(bu["nbEventsMissingData"])
            dashboards.gauge_bu_nb_events_with_crc_errors.labels(
                name, bus_rus_map[name][2]).set(bu["nbEventsWithCRCerrors"])
            dashboards.gauge_bu_nb_total_resources.labels(
                name, bus_rus_map[name][2]).set(bu["nbTotalResources"])
            dashboards.gauge_bu_request_rate.labels(
                name, bus_rus_map[name][2]).set(bu["requestRate"] / 1000)
            dashboards.gauge_bu_request_retry_rate.labels(
                name, bus_rus_map[name][2]).set(bu["requestRetryRate"] / 1000)
            dashboards.gauge_bu_slowest_ru_tid.labels(
                name, bus_rus_map[name][2]).set(bu["slowestRUtid"])

    for ru in snapshot["rus"]:
        fed_builder = ru["ref_fedBuilder"].split('_')[1]
        hostname = ((ru["hostname"].split("-")[1]+"-" +
                    ru["hostname"].split("-")[2]).split(".")[0])[2:]
        if ru["stateName"] != "Enabled":
            dashboards.gauge_ru_rate.labels(hostname, fed_builder).set(-1)
            dashboards.gauge_ru_throughput.labels(
                hostname, fed_builder).set(-1)
            dashboards.gauge_ru_superfragment_size_mean.labels(
                hostname, fed_builder).set(-1)
            dashboards.gauge_ru_superfragment_size_stddev.labels(
                hostname, fed_builder).set(-1)
            dashboards.gauge_ru_fragments_in_ru.labels(
                hostname, fed_builder).set(-1)
            dashboards.gauge_ru_events.labels(hostname, fed_builder).set(-1)
            dashboards.gauge_ru_event_count.labels(
                hostname, fed_builder).set(-1)
            dashboards.gauge_ru_requests.labels(hostname, fed_builder).set(-1)
            dashboards.gauge_ru_incomplete_super_fragment_count.labels(
                hostname, fed_builder).set(-1)
        else:
            dashboards.gauge_ru_rate.labels(
                hostname, fed_builder).set((ru["rate"]) / 1000)
            dashboards.gauge_ru_throughput.labels(
                hostname, fed_builder).set((ru["throughput"]) / 1000000)
            dashboards.gauge_ru_superfragment_size_mean.labels(
                hostname, fed_builder).set((ru["superFragmentSizeMean"]) / 1000)
            dashboards.gauge_ru_superfragment_size_stddev.labels(
                hostname, fed_builder).set((ru["superFragmentSizeStddev"]) / 1000)
            dashboards.gauge_ru_fragments_in_ru.labels(
                hostname, fed_builder).set((ru["fragmentsInRU"]))
            dashboards.gauge_ru_events.labels(
                hostname, fed_builder).set((ru["eventsInRU"]))
            dashboards.gauge_ru_event_count.labels(
                hostname, fed_builder).set((ru["eventCount"]))
            dashboards.gauge_ru_requests.labels(
                hostname, fed_builder).set((ru["requests"]))
            dashboards.gauge_ru_incomplete_super_fragment_count.labels(
                hostname, fed_builder).set((ru["incompleteSuperFragmentCount"]))

    #  -------------------------------- feds --------------------------------------------------------------

    
    # print(f"Current deadtime (%): {deadtime_instant_percent} ")
    # print(f"LV0 state: {snapshot['levelZeroState']}")
    # print(f"DAQ state: {snapshot['daqState']}")
    # print(f"Current Lv1 rate: {current_lv1_rate_khz} kHz")
    # print(f"Events since start of run: {accumulated_lv1_event_count:e}")
    # print()
    # print(
    #     f"hltRate: {snapshot['hltRate']}, TRG rate: {snapshot['tcdsGlobalInfo']['trg_rate_total']}")
    # print(
    #     f"FB Rate: {snapshot['fedBuilderSummary']['rate']}, BU Rate: {snapshot['buSummary']['rate']}")

    # # get subsystem table
    # # not all subsystem are in the subSystems list
    # # not yet added: L1SCOUT
    # # not included (on purpose): DAQ, DCS, DQM
    # print(f"{'Subsystem':<15}{'Mode':<5}{'State':<10}")
    # print(f"{'DAQ':<15}{'In':<5}{snapshot['daqState']:<10}")
    # for subsystem in snapshot['subSystems']:
    #     name = subsystem['name']
    #     status = subsystem['status']
    #     mode = "Out" if status == 'Faulty' or status == '' else "In"

    #     displayed_status = status if not mode == 'Out' else ''

    #     print(f"{name:<15}{mode:<5}{displayed_status:<10}")

    
if __name__ == '__main__':
    start_http_server(8000)

    try:
        response = urllib.request.urlopen(url)
    except HTTPError as e:

        logging.error(f"Error code: {e.code}")
    except URLError as e:
        logging.error(f"Reason: {e.reason}")

    else:
        snapshot = json.loads(response.read())
        last_processed_snapshot_timestamp = -1

        # using this variable in order to initialize only once the feds_dict_sec with -1
        first_run = True
        # fetching the feds_dict_sec only once, this should change with whenever we have a new run number
        

        # current_run_number = snapshot["runNumber"]
    
        while True:
            response = urllib.request.urlopen(url)
            snapshot = json.loads(response.read())
            snapshot_timestamp = snapshot['lastUpdate']
            current_run_number = snapshot["runNumber"]

            # if last_run_number != current_run_number:
            #     first_run = True
            #     feds_dict_sec = utils.fetch_ref_ttcp(snapshot, "empty_dictionary")

            # last_run_number = current_run_number
            # print(current_run_number)
            # if we have alregitady processed this snapshot, just wait some time and try again
            if last_processed_snapshot_timestamp >= snapshot_timestamp:
                # print("Got the same snapshot again! Not processing...")
                # gauge_not_new_snapsot.set(1000000)
                time.sleep(poll_time_seconds)
                continue

            # calculating the time for the rate in seconds
            delta_time = 0.001 * (snapshot_timestamp -
                                last_processed_snapshot_timestamp)

            last_processed_snapshot_timestamp = snapshot_timestamp
            # gauge_snapshot_timestamp.set(snapshot_timestamp)
            # print(
            #     f"Found new snapshot from: {datetime.datetime.utcfromtimestamp(snapshot_timestamp/1000).isoformat()} UTC")

            start_time = datetime.datetime.now()
            

            work_snapshot(snapshot, first_run)
            first_run = False
            # sleep for the remainder of time
            processing_time_seconds = (
                datetime.datetime.now() - start_time).total_seconds()
            if processing_time_seconds < poll_time_seconds:
                time.sleep(poll_time_seconds - processing_time_seconds)
