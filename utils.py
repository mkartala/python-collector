#!/usr/bin/env python3
"""
This module is created for HTTP requests to Grafana annotation API
You first have to get an API-KEY from the dasboard you are interested and you also need
to get the dasboard unique id and the panel's id you want to work with
Urrlib3 needs the body of the HTTP request encoded to json. So you first have to call
create_encoded_body and then make the request you want.
"""
import urllib3
import json
http = urllib3.PoolManager()

response = ""


def make_post_request(encoded_body, url, token):
    global response
    """
    This function create a post_request to grafana annotations api
    It needs the body of the request encoded as json, where time and timeEnd  arein milleseconds
    """
    http = urllib3.PoolManager()

    response = http.request("POST", url, headers={"Content-Type": "application/json", "Authorization": token, "Accept": "application/json"},
                            body=encoded_body)

    if response.status != 200:
        print("Annotation POST request failed: ")
        print(json.dumps(json.loads(response.data.decode("utf-8")), indent=2))
    else:
        return "Annotation POST request succeed"


def delete_annotation(encoded_body, url, token, annotation_id):
    global response
    """
    This functions is for deleting an existing annotation
    """

    url_with_id = url + "/" + str(annotation_id)

    response = http.request("DELETE", url_with_id, headers={'Content-Type': 'application/json',
                                                            'Authorization': token,
                                                            'Accept': 'application/json'})
    if response.status != 200:
        print("Annotation DELETE request failed: ")
        print(json.dumps(json.loads(response.data.decode("utf-8")), indent=2))
    else:
        print("Annotation DELETE request succeed")


def get_annotations(url, token):
    """
    This function create a GET requests to the API and gets back all the 
    annotations
    """
    response = http.request("GET", url, headers={'Content-Type': 'application/json',
                                                 'Authorization': token,
                                                 'Accept': 'application/json'})
    if response.status != 200:
        print("Annotation GET request failed: ")
        print(json.dumps(json.loads(response.data.decode("utf-8")), indent=2))
    elif response.status == 200:
        print(json.dumps(json.loads(response.data.decode("utf-8")), indent=2))
    else:
        if (len(response.data) == 2):
            print("No annotations found! ")


def create_encoded_body(method: str, dashboardUID: str, panelId: int, tags="", description=" ", time: int = 0, timeEnd: int = 0):
    """
    This function create the body of the request you ara going to make. Urrlib3 library need the 
    body of the requests formated as json. This function dependind on the method of the requests 
    creates the correct encoded_body
    """

    if method == "POST":
        if timeEnd == 0:
            timeEnd = time
        return json.dumps({"dashboardUID": dashboardUID, "panelId": panelId, "time": time, "timeEnd": timeEnd, "tags": [tags], "text": description})

    elif method == "GET":
        return "GET request does not need a encoded_body :) "

    elif method == "DELETE":
        return json.dumps({"dasboardUID": dashboardUID, "panelId": panelId})

    # to be implemented
    elif method == "PATCH":
        pass

    return "No other request method supported"


def check_zero_value(value):
    """
    This function checks if the value is zero. If it is zero it returns true
    """
    if value == 0:
        return True
    else:
        return False


#  ------------------------------------ fetching things from json ---------------------------------------------------------


def fetch_ref_ttcp(snapshot, re_type):
    """
    This function retrieves all the feds used and returns a dictionary with them as keys with value a list or another dictionary
    this is implemented in order to be able to add nested dictionaries with the srcIdexpected that help calculate the rate of 
    the accFlinksecc
    """
    feds  = set()
    feds_dict = dict()

    for fed in snapshot["feds"]:
        
        feds.add(fed["ref_ttcp"].split("_")[1])
        
    
    if re_type == "empty_list":
        for fed in feds:
            feds_dict.update({fed:[]})

    elif re_type == "empty_dictionary":
        for fed in feds:
            feds_dict.update({fed:{}})
    
    
    return feds_dict

def fetch_srcid_for_each_ttcp(snapshot,feds_dict):
    """
    This functions get all the sourcses ids and updates a dictionary with the feds
    """
    
    
    for fed in snapshot["feds"]:
        ref_ttcp = fed["ref_ttcp"].split("_")[1]
        # if fed["frlMasked"] == False:
        feds_dict[ref_ttcp] += [fed["srcIdExpected"]]
    
    
    return feds_dict

def fetch_from_feds_percents(snapshot,src_id_expected):
    """
    This function returns the percentWarning and the percentBusy
    """
    for fed in snapshot["feds"]:
        if fed["srcIdExpected"] == src_id_expected and (fed["frlMasked"] == False or fed["fmmMasked"] == False):
           return fed["percentWarning"], fed["percentBusy"]
    
    return -1,-1

def fetch_fed_values(snapshot,src_id_expected):
    '''
    This function is returing values from fed with srcIdExpected == src_id_expected
    '''
    values_from_fed = {
        "frl_AccSlinkFullSec" :-1,
        "frl_AccBackpressureSeconds":-1,
        "frl_AccBIFIBackpressureSeconds":-1,
        "frl_bifi_used":-1,
        "frl_tcp_stat_persist_closedwnd": -1,
        "frl_tcp_stat_current_wnd": -1,
        "frl_tcp_stream_timestamp": -1,
        "frl_input_stream_timestamp":-1,
        "frl_slink_rcv_fifo_cnt_transitions":-1,
        "frl_tcpstream_bifi_cnt_transitions":-1,
        "eventCounter":-1
    }
    
    for fed in snapshot["feds"]:
        if fed["srcIdExpected"] == src_id_expected and fed["frlMasked"] == False:
            values_from_fed["frl_AccSlinkFullSec"] = fed["frl_AccSlinkFullSec"]
            values_from_fed["frl_AccBackpressureSeconds"] = fed["frl_AccBackpressureSeconds"]
            values_from_fed["frl_AccBIFIBackpressureSeconds"] = fed["frl_AccBIFIBackpressureSeconds"]
            values_from_fed["frl_bifi_used"] = fed["frl_bifi_used"]
            values_from_fed["frl_tcp_stat_persist_closedwnd"] = fed["frl_tcp_stat_persist_closedwnd"]            
            values_from_fed["frl_tcp_stat_current_wnd"] = fed["frl_tcp_stat_current_wnd"]
            values_from_fed["frl_tcp_stream_timestamp"] = fed["frl_tcp_stream_timestamp"]
            values_from_fed["frl_input_stream_timestamp"] = fed["frl_input_stream_timestamp"]
            values_from_fed["frl_slink_rcv_fifo_cnt_transitions"] = fed["frl_slink_rcv_fifo_cnt_transitions"]
            values_from_fed["frl_tcpstream_bifi_cnt_transitions"] = fed["frl_tcpstream_bifi_cnt_transitions"]
            values_from_fed["eventCounter"] = fed["eventCounter"]
            
            return values_from_fed

    return values_from_fed   

def fetch_fmm_counters(snapshot,src_id_expected):
    
    for fed in snapshot["feds"]:
        if fed["srcIdExpected"] == src_id_expected and fed["frlMasked"] == False:
            
            return [fed["fmm_integralTimeReady_bx"],fed["fmm_integralTimeBusy_bx"],fed["fmm_integralTimeWarning_bx"],fed["fmm_integralTimeError_bx"],fed["fmm_integralTimeOOS_bx"],fed["fmm_timeTag_bx"]]
    return -1

def initialize_feds_rates_dict(feds_dict_sec,snapshot):
    # for key, value in feds_dict.items():
    #         for i in range(0, len(value)):
               
    #             feds_dict_sec[key][value[i]] = -1
    for fed in snapshot["feds"]:
        feds_dict_sec[fed["ref_ttcp"].split("_")[1]][fed["srcIdExpected"]] = -1
    return (feds_dict_sec)

def fetch_tcds_global_info_deadtimes_instant(deadTimesInstant,key):

    if key in deadTimesInstant:
        return deadTimesInstant[key]
    else:
        return -1

def fetch_tcds_global_info(tcdsGlobalInfo,key):
    if key in tcdsGlobalInfo:
        return tcdsGlobalInfo[key]
    else:
        return -1
def add_to_the_sum(value_to_add):

    if value_to_add == -1:
        return 0
    else:
        return  value_to_add

def calculate_rate(current_value,prev_value,delta_time):

    if current_value == 0:
        return  0
    else:
        return (current_value - prev_value) / delta_time
        
def update_current_max(value,current_max,src_id_expected,old_fed):
 
    if value > current_max:
        
        
        return value , src_id_expected

    else:
        return current_max,old_fed

def sort_alphnum( l ): 
    """ Sort the given iterable in the way that humans expect. you must import r """ 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)