from prometheus_client import Counter, Gauge, start_http_server

# Variables not dasboard specifi
gauge_run_number = Gauge("run_number", "The run number")
gauge_last_update_timestamp = Gauge("last_timestamp","The unix timestamp of the last update")
# ------------------------------------------------TCDS DASHBOARD---------------------------------------------------------------

gauge_tcds_pm_ttsState = Gauge("tcds_pm_ttsState","tcds_pm_ttsState for the ttcPartitions",["hostname"])
gauge_ttc_partition_warning_and_busy = Gauge("ttc_partition_warning_and_busy","The sum of warning and busy percentage",["hostname"])
#  first panel
gauge_tcds_instant_trg_rate_physics_khz = Gauge(
    "tcds_instant_trg_rate_physics_khz", "tcds_instant_trg_rate_physics_khz")
gauge_tcds_instant_trg_rate_seq_calib_khz = Gauge(
    "tcds_instant_trg_rate_seq_calib_khz", "tcds_instant_trg_rate_seq_calib_khz")
gauge_tcds_instant_trg_rate_random_khz = Gauge(
    "tcds_instant_trg_rate_random_khz", "tcds_instant_trg_rate_random_khz")
gauge_tcds_instant_trg_rate_total_khz = Gauge(
    "tcds_instant_trg_rate_total_khz", "gauge_tcds_instant_trg_rate_total_khz")


# second panel
gauge_tcds_instant_deadtimes_total = Gauge(
    "tcds_instant_deadtimes_total", "tcds_instant_deadtimes_total")
gauge_tcds_instant_deadtimes_bx_mask = Gauge(
    "tcds_instant_deadtimes_bx_mask", "tcds_instant_deadtimes_bx_mask")
gauge_tcds_instant_deadtimes_trg_rules = Gauge(
    "tcds_instant_deadtimes_trg_rules", "tcds_instant_deadtimes_trg_rules")
gauge_tcds_instant_deadtimes_calib = Gauge(
    "tcds_instant_deadtimes_calib", "tcds_instant_deadtimes_calib")
gauge_tcds_instant_deadtimes_daq_bp = Gauge(
    "tcds_instant_deadtimes_daq_bp", "tcds_instant_deadtimes_daq_bp")
gauge_tcds_instant_deadtimes_retri = Gauge(
    "tcds_instant_deadtimes_retri", "tcds_instant_deadtimes_retri")
gauge_tcds_instant_deadtimes_tts = Gauge(
    "tcds_instant_deadtimes_tts", "tcds_instant_deadtimes_tts")
gauge_tcds_instant_deadtimes_apve = Gauge(
    "tcds_instant_deadtimes_apve", "tcds_instant_deadtimes_apve")
gauge_tcds_instant_deadtimes_fw_pause = Gauge(
    "tcds_instant_deadtimes_fw_pause", "tcds_instant_deadtimes_fw_pause")
gauge_tcds_instant_deadtimes_sw_pause = Gauge(
    "tcds_instant_deadtimes_sw_pause", "tcds_instant_deadtimes_sw_pause")


#  -----------------------------------------  Summary Dashboard ---------------------------------------------------------------
#  hlt
gauge_hlt_rate_Hz = Gauge("hlt_rate_Hz", "hlt_rate_Hz")
gauge_hlt_out_thru_GBps = Gauge("hlt_out_thru_GBps", "hlt_out_thru_GBps")
gauge_hlt_accept_fraction = Gauge("hlt_accept_fraction", "htl_accept_fraction")


# History (Ls Base) panel
gauge_tcds_section_rates_trg_rate_total = Gauge(
    "tcds_section_rates_trg_rate_total", "The tcds_section_rates_trg_rate_total")
gauge_tcds_section_counts_trg_total_accumulated = Gauge(
    "tcds_section_counts_trg_total_accumulated", "The accumulated value of trg_total for all lumi-sections in a run")
gauge_tcds_section_deadtimes_total = Gauge(
    "tcds_section_deadtimes_total", "The tcds_section_deadtimes_total")


# live panel

gauge_lv1_rate_khz = Gauge("current_lv1_rate", "lv1 rate in khz")
gauge_deadtime_instant_percent = Gauge(
    "dead_time_instant_percent", "Dead time instant percent")
gauge_accumulated_lv1_events = Gauge(
    "accumulated_lvl1_event_count", "lv1 events counter")

# ---------------------------------------------- Fran's Dasboard -------------------------------------------------------------------

gauge_tcds_section_rates_ls = Gauge(
    "tcds_section_rates_ls", "The tcds_section_rates_ls")
gauge_tcds_section_counts_ls = Gauge(
    "tcds_section_counts_ls", "The tcds_section_counts_ls")
gauge_tcds_section_deadtime_ls = Gauge(
    "tcds_section_deadtime_ls", "The tcds_section_deadtime_ls")
gauge_tcds_section_counts_trg_cnt_total = Gauge(
    "tcds_section_trg_cnt_total", "The total tcds_section_trg_cnt")

# ------------------------------ Event builder Dashboard   ---------------------------------------------------------------------------

#  --------------------- RUS ---------------------------------------------------------------------------------------------------------
gauge_ru_rate = Gauge("ru_rate", "Reate of the ru",["hostname","fed_builder"])
gauge_ru_throughput = Gauge("ru_throughput", "Throughput in  a ru",["hostname","fed_builder"])
gauge_ru_superfragment_size_mean = Gauge("ru_superfragment_size_mean","Super fragment size mean in a ru",["hostname","fed_builder"])
gauge_ru_superfragment_size_stddev = Gauge("ru_superfragment_size_stddev", "The size of superfragment",["hostname","fed_builder"])
gauge_ru_fragments_in_ru = Gauge("ru_fragments","Fragments in ru",["hostname","fed_builder"])
gauge_ru_events = Gauge("ru_events","Events in ru",["hostname","fed_builder"])
gauge_ru_event_count = Gauge("ru_event_count", "The count of the event of a ru",["hostname","fed_builder"])
gauge_ru_requests = Gauge("ru_requests","Requests in ru",["hostname","fed_builder"])
gauge_ru_incomplete_super_fragment_count = Gauge("ru_incomplete_super_fragment_count", "The incomplete_super_fragment count",["hostname","fed_builder"])


#  --------------------BUS -------------------------------------------------------------------------------------------------------------------

gauge_bu_rate = Gauge("bu_rate","Rate of the bu",["hostname","fed_builder"])
gauge_bu_throughput = Gauge("bu_throughput","Throughput in a bu",["hostname","fed_builder"])
gauge_bu_event_size_mean = Gauge("bu_event_size_mean","The size of event",["hostname","fed_builder"])
gauge_bu_event_size_stddev = Gauge("bu_event_size_stddev","The size of sttdev",["hostname","fed_builder"])
gauge_bu_event_num_events = Gauge("bu_num_event","Number of events bu",["hostname","fed_builder"])
gauge_bu_event_num_events_in_bu = Gauge("bu_num_events_in_bu","Number of events in a bu",["hostname","fed_builder"])
gauge_bu_priority = Gauge("bu_priority","Bu priority",["hostname","fed_builder"])
gauge_bu_num_requests_sent	= Gauge("bu_num_requests_sent","Reqeusts sent in bu",["hostname","fed_builder"])
gauge_bu_num_requests_used	= Gauge("bu_num_requests_used","Requests used in bu",["hostname","fed_builder"])
gauge_bu_num_requests_blocked	= Gauge("bu_num_requests_blocked","Requests blocked in bu",["hostname","fed_builder"])
gauge_bu_num_fus_hlt = Gauge("bu_num_fus_hlt","bu num fus hlt",["hostname","fed_builder"])
gauge_bu_num_fus_crashed = Gauge("bu_num_fus_crashed","number of fus crashed",["hostname","fed_builder"])
gauge_bu_num_fus_stale = Gauge("bu_num_fus_stale","number of fus stale",["hostname","fed_builder"])
gauge_bu_num_fus_cloud = Gauge("bu_num_fus_cloud","number of fus cloud",["hostname","fed_builder"])
gauge_bu_ram_disk_usage = Gauge("bu_ram_disk_usage","bu ram disk usage",["hostname","fed_builder"])
gauge_bu_ram_disk_total = Gauge("bu_ram_disk_total","bu ram disk total",["hostname","fed_builder"])
gauge_bu_num_files = Gauge("bu_num_files","bu num files",["hostname","fed_builder"])
gauge_bu_num_lumisections_with_files = Gauge("bu_num_lumisections_with_files","numer lumisections ",["hostname","fed_builder"])
gauge_bu_current_lumisection = Gauge("bu_current_lumisection","bu current lumisection",["hostname","fed_builder"])
gauge_bu_num_lumisections_for_hlt = Gauge("bu_num_lumisections_for_hlt","bu lumisection for hlt",["hostname","fed_builder"])
gauge_bu_num_lumisections_out_hlt = Gauge("bu_num_lumisections_out_hlt","bu_lumisection of hlt",["hostname","fed_builder"])
gauge_bu_fu_output_bandwidth_in_mb = Gauge("bu_fu_output_bandwidth_in_mb","bu fu bandwidth outpu",["hostname","fed_builder"])
gauge_bu_fragment_count = Gauge("bu_fragment_count","bu fragment count",["hostname","fed_builder"])
gauge_bu_nb_corrupted_events = Gauge("bu_nb_corrupted_events","bu nb corrupted events",["hostname","fed_builder"])
gauge_bu_nb_events_missing_data = Gauge("bu_nb_events_missing_data","nb events missing data",["hostname","fed_builder"])
gauge_bu_nb_events_with_crc_errors = Gauge("bu_nb_events_with_crc_errors","bu events with crc errors",["hostname","fed_builder"])
gauge_bu_nb_total_resources = Gauge("bu_nb_total_resources","bu nb rotal",["hostname","fed_builder"])
gauge_bu_request_rate = Gauge("bu_request_rate","bu request rate",["hostname","fed_builder"])
gauge_bu_request_retry_rate = Gauge("bu_request_retry_rate","bu request retry",["hostname","fed_builder"])
gauge_bu_slowest_ru_tid = Gauge("bu_slowest_ru_tid","bu slowest ru id",["hostname","fed_builder"])



#  ------------------------------------ Feds  -------------------------------------------------------------------------------

gauge_ttcp_frl_acc_sec_sum_per_partition = Gauge("ttcp_frl_acc_sec_per_partition","Sum of all the srcIdsExpected",["ref_ttcp"])
gauge_ttcp_frl_acc_bifi_pre_sum_per_partition = Gauge("ttcp_frl_acc_bifi_pre_sum_per_partition","Sum of all the srcIdsExpected",["ref_ttcp"])
gauge_ttcp_frl_acc_back_pre_sum_per_partition = Gauge("ttcp_frl_acc_back_pre_sum_per_partition","Sum of all the srcIdsExpected",["ref_ttcp"])

gauge_ttcp_fmm_integral_time_ready_bx = Gauge("ttcp_fmm_integral_time_ready_bx","integral time ready",["ref_ttcp","src_id_expected"])
gauge_ttcp_fmm_integral_time_busy_bx = Gauge("ttcp_fmm_integral_time_busy_bx","integral time busy",["ref_ttcp","src_id_expected"]) 
gauge_ttcp_fmm_integral_time_warning_bx = Gauge("ttcp_fmm_integral_time_warning_bx","integral_time_warning",["ref_ttcp","src_id_expected"]) 
gauge_ttcp_fmm_integral_time_error_bx = Gauge("ttcp_fmm_integral_time_error","integral time error",["ref_ttcp","src_id_expected"]) 
gauge_ttcp_fmm_integral_time_oss_bx = Gauge("ttcp_fmm_integral_time_oss_bx","integral time oss",["ref_ttcp","src_id_expected"])
gauge_ttcp_fmm_time_tag_bx = Gauge("ttcp_timeTag_bx","integral timetag",["ref_ttcp","src_id_expected"])

gauge_ttcp_frl_acc_sec = Gauge("ttcp_frl_acc_sec","The acc slink full sec",["ref_ttcp","src_id_expected"])
gauge_ttcp_frl_acc_bifi_pre_sec = Gauge("ttcp_frl_acc_bifi_pre_sec","the acc backpressure sec",["ref_ttcp","src_id_expected"])

gauge_ttcp_frl_acc_back_pres_sec = Gauge("ttcp_frl_back_pre_sec","The acc back pressure seconds",["ref_ttcp","src_id_expected"])
gauge_ttcp_frl_slink_rcv_fifo_cnt_transitions = Gauge("ttcp_frl_slink_rcv_fifo_cnt_transitions","Transitions rcv",["ref_ttcp","src_id_expected"])
gauge_ttcp_frl_tcpstream_bifi_cnt_transitions = Gauge("ttcp_frl_tcpstream_bifi_cnt_transitions","Transitions rcv",["ref_ttcp","src_id_expected"])

gauge_ttcp_frl_rate_slink_full = Gauge("ttcp_frl_rate_slink_full", "the rate of the accFullSlink",["ref_ttcp","src_id_expected"])
gauge_ttcp_frl_rate_bifi_pre_sec = Gauge("ttcp_frl_rate_bifi_pre_sec","The rate of the bifibackpressure",["ref_ttcp","src_id_expected"])
gauge_ttcp_frl_rate_back_pre_sec = Gauge("ttcp_frl_rate_back_pre_sec","The rate of frlaccbackpressure",["ref_ttcp","src_id_expected"])

gauge_ttcp_percent_warning = Gauge("ttcp_percent_warning","The percentage of warning for each srcid",["ref_ttcp","src_id_expected"])
gauge_ttcp_percent_busy = Gauge("ttcp_percent_busy","The percentage of warning for each srcid",["ref_ttcp","src_id_expected"])
gauge_ttcp_max_incremented_delta_value_frl_acc_sec = Gauge("ttcp_max_incremented_delta_value_frl_acc_sec","this is the maximum incremented frl_AccSlinkFullsec",["ref_ttcp"])
gauge_ttcp_max_incremented_delta_value_back_pre_sec = Gauge("ttcp_max_incremented_delta_value_back_pre_sec","this is the maximum incremented frl_AccBackpressureSeconds",["ref_ttcp"])
gauge_ttcp_max_incremented_delta_value_bifi_back_pre_sec = Gauge("ttcp_max_incremented_delta_value_bifi_back_pre_sec","this is the maximum incremented frl_AccBIFIBackpressureSeconds",["ref_ttcp"])

gauge_ttcp_acc_max_incremented_delta_value_frl_acc_sec = Gauge("ttcp_acc_max_incremented_delta_value_frl_acc_sec","this is the maximum incremented frl_AccSlinkFullsec",["ref_ttcp"])
gauge_ttcp_acc_max_incremented_delta_value_back_pre_sec = Gauge("ttcp_acc_max_incremented_delta_value_back_pre_sec","this is the maximum incremented frl_AccBackpressureSeconds",["ref_ttcp"])
gauge_ttcp_acc_max_incremented_delta_value_bifi_back_pre_sec = Gauge("ttcp_acc_max_incremented_delta_value_bifi_back_pre_sec","this is the maximum incremented frl_AccBIFIBackpressureSeconds",["ref_ttcp"])

gauge_fed_eventcounter = Gauge("fed_eventcouter","this is the event counter that each fed has",["ref_ttcp","src_id_expected"])
gauge_fed_frl_bifi_used = Gauge("fed_frl_bifi_used", "This is the bifi used",["ref_ttcp","src_id_expected"])
gauge_fed_frl_tcp_stat_persist_closedwnd = Gauge("fed_frl_tcp_stat_persist_closedwnd", "This is the fed_frl_tcp_stat_persist_closedwnd",["ref_ttcp","src_id_expected"])
gauge_fed_frl_tcp_stat_current_wnd = Gauge("fed_frl_tcp_stat_current_wnd", "This is the fed_frl_tcp_stat_current_wnd",["ref_ttcp","src_id_expected"])

gauge_fed_frl_tcp_stream_timestamp = Gauge("fed_frl_tcp_stream_timestamp", "This is the fed_frl_tcp_stat_current_wnd",["ref_ttcp","src_id_expected"])
gauge_fed_frl_input_stream_timestamp = Gauge("fed_frl_input_stream_timestamp","This is the fed_frl_tcp_stream_timestamp",["ref_ttcp","src_id_expected"])
