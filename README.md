# python-collector

Python scripts for collecting data from DAQ aggregator snaposhots and pushing them to Prometheus instance

## Getting started

This project comes with a .env file. You can find an example of the structure of it, that the collect-basi.py expects in the root directory, called .env.example
